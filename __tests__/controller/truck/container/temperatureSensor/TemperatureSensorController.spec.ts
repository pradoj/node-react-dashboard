import { app, serverIO } from '../../../../../server/app';
const request = require('supertest');

describe('Test temperature sensor controller', () => {
    test('It should response the POST method', () => {
        return request(app).put('/api/v1/truck/containers/0324aad0-336c-11e9-b877-1b9ad917432a/temperatureSensor').expect(200);
    });

    test('It should response the POST method with 404 when container does not exist', () => {
        return request(app).put('/api/v1/truck/containers/1/temperatureSensor').expect(404);
    });
});


afterAll((done) => {
    return  serverIO && serverIO.close(done);
  });
