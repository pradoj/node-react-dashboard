import { app, serverIO } from '../../../server/app';
const request = require('supertest');

describe('Test truck controller', () => {
    test('It should response the GET method', () => {
        return request(app).get('/api/v1/truck/containers').expect(200);
    });
});

afterEach((done) => {
    return  serverIO && serverIO.close(done);
  });
