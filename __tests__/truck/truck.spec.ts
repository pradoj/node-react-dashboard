import { Truck } from "../../server/truck/truck";
import { Container } from '../../server/truck/container/container';

describe('truck ', () => {
    it('create a truck', () => {
        let truck = Truck.getInstance();
        expect(truck).not.toBeNull();      
    });

    it('Truck can have multiple containers', () => {
        let truck = Truck.getInstance();
        truck.addContainer(new Container(0,2));
        truck.addContainer(new Container(3,4));
        expect(Array.from(truck.containers.values()).length).toBe(2);
    });

    it('Truck should emit alerts of its containers', (done) => {
        let truck = Truck.getInstance();
        truck.addContainer(new Container(2,5,'1'));
        truck.addContainer(new Container(3,7,'2'));
        truck.containers.get('1').temperatureSensor.value =  4;        

        let alertVerified = false;
        let changeVerified = false;
        truck.truckEvents.on(Truck.EVENT_ALERT, function(_id, active){
            expect(active).toBeTruthy();
            alertVerified = true;
            if (changeVerified){
                done();
            }
          });
          truck.truckEvents.on(Truck.EVENT_CHANGE, function(attribute){
            expect(attribute).toBe(Truck.EVENT_CHANGE_ATT_TEMPERATURE);
            changeVerified = true;
            if (alertVerified){
                done();
            }
        });
        truck.containers.get('1').temperatureSensor.value =  6;

    });   
});
