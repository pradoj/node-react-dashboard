import { Container } from '../../../server/truck/container/container';
import { ThresholdEnum } from '../../../server/truck/container/thresholdEnum';

describe('container ', () => {
    it('create a container', () => {
        let container = new Container(3,6);
        expect(container).not.toBeNull();
        expect(container.id).toBeDefined();
      });

      it('Container should have temperature', (done) => {
        let container = new Container(4,6);
                
        expect(container.maxTemperature).toBe(6);
        expect(container.minTemperature).toBe(4);

        container.temperatureSensor.value = 5;

        expect(container.temperature).toBe(5);
        done()      

      });

      it('Container should notify temperature changes', (done) => {
        let container = new Container(4,6);
                
        expect(container.maxTemperature).toBe(6);
        expect(container.minTemperature).toBe(4);
      
        container.temperatureSensor.value = 5;
        container.temperatureSensor.sensorEvents.on('change', function(value, id){
          expect(value).toBe(7);
          expect(id).toBe(container.id);
          done();
        })
        container.temperatureSensor.value = 7;
      });

      it('Container should trigger temperature alert', (done) => {
        let container = new Container(4,6);
                
        expect(container.maxTemperature).toBe(6);
        expect(container.minTemperature).toBe(4);
        
        container.alerts.get(ThresholdEnum.max).alertEvents.on('alertOn',function(id){
          expect(container.temperature).toBe(7);
          expect(container.id).toBe(id);
          expect(container.alarm).toBe(true);
          container.temperatureSensor.value = 4;
        });

        container.alerts.get(ThresholdEnum.max).alertEvents.on('alertOff',function(id){
          expect(container.temperature).toBe(4);
          expect(container.id).toBe(id);
          expect(container.alarm).toBe(false);
          done();
        });
        var changed = false;
       
        container.temperatureSensor.sensorEvents.on('change',function(){
          changed = true
        });
        container.temperatureSensor.value = 5;
        expect(container.temperature).toBe(5);
        expect(changed).toBe(true);
        expect(container.alarm).toBe(false);
         
        container.temperatureSensor.value = 7;   
        
      });
});