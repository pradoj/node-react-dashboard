import { TruckController } from './controllers/truck/truckController';
import { Router } from 'express';

import { TemperatureSensorController } from './controllers/truck/container/temperatureSensor/temperatureSensorController';


export class Routes{
    
    constructor(public router: Router){
        const truckController = new TruckController();  
        const temperatureSensorController = new TemperatureSensorController();  
        
        this.router.get('/api/v1/truck/containers',
        truckController.getAllContainer)

        this.router.put('/api/v1/truck/containers/:containerId/temperatureSensor',
        temperatureSensorController.updateTemperature)
    }

}