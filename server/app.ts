import { TruckServer } from './truckServer';
const truckServer = TruckServer.getInstance();
let app = truckServer.getApp();
let serverIO = truckServer.getIOServer();
export { app, serverIO };