

import * as express from 'express';

import { Routes } from './routes';
import { Truck } from './truck/truck';

import * as bodyParser from "body-parser";
import { Server } from 'http';

import { Container } from './truck/container/container';
import { TruckSocket } from './truck/truckSocket';


export class TruckServer {
    public static readonly EVENT_ALL_CONTAINERS = 'containers.all';
    public static readonly PORT: number = 3000;
    public static getInstance(){
        if (!TruckServer.instance) {         
            TruckServer.instance = new TruckServer();
        }
        return TruckServer.instance;
    }
    private static instance: TruckServer;
    readonly path = require('path')

    readonly containersConfig = require('./config/containers')

    private app: express.Application = express();
    private server: Server = require('http').Server(this.app);
    private io: SocketIO.Server = require('socket.io')(this.server)

    private constructor() {
        this.configApp();
        this.loadRoutes();
        this.loadPreConfig();
        this.sockets();
    }

    private configApp() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(express.static('public'))
        this.app.use(new Routes(express.Router()).router);
        this.server.listen(TruckServer.PORT, () => {
            console.log(`server running on port ${TruckServer.PORT}`);
        });

    }

    private loadRoutes() {
        this.app.get('/', (_req, res) => res.sendFile(this.path.join(__dirname, 'index.html')))
    }


    private sockets(): void {

        this.io.on('connection', socket => {
            const truckSocket: TruckSocket =  TruckSocket.getInstance(this.io);;

            socket.on(TruckServer.EVENT_ALL_CONTAINERS, () => {
                truckSocket.emitAllContainers();
            })
        
        })
    }


    private loadPreConfig() {
        const truck = Truck.getInstance();
        this.containersConfig.forEach((containerConfig) => {
            const container = new Container(containerConfig.minTemperature,
                containerConfig.maxTemperature, containerConfig.id)
            container.temperatureSensor.value = Number(containerConfig.minTemperature) + 1;
            truck.addContainer(container);
        });

    }
    public getApp(): express.Application {
        return this.app;
    }
    public getIOServer(): SocketIO.Server {
        return this.io;
    }
}
