
import { Truck } from '../../truck/truck';
import { Request, Response } from 'express';
import { Container } from '../../truck/container/container';

export class TruckController{
    public getAllContainer(_req: Request, res: Response) {
              
        const containers:Array<Container> = Array.from(Truck.getInstance().containers.values());
        res.status(200).send({
            success: 'true',
            message: 'containers retrieved successfully',
            containers
        })
    }
}