import { Truck } from '../../../../truck/truck';
import { Request, Response } from 'express';
import { Container } from '../../../../truck/container/container';

export class TemperatureSensorController{
    
    public updateTemperature(req: Request, res:Response) {

        const container:Container = Truck.getInstance().getContainer(req.params.containerId);        
        if (container == undefined){
            res.status(404).send({
                success: 'false',
                message: 'container not found'
            });
            
        }else{
            container.temperatureSensor.value = req.body.value;
            res.status(200).send({
                success: 'true',
                message: 'temperature has been updated successfully',
                container
            })
        }
    }
}