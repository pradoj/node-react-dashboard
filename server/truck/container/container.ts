import { ThresholdEnum } from './thresholdEnum';
import { Threshold } from './threshold';
import { Sensor } from './sensor';
import { Alert } from './alert';
import { v1 as uuid } from 'uuid';
import { EventEmitter } from 'events';
import { AlertInterface } from './alertInterface';
import { SensorInterface } from './sensorInterface';

export class Container {
    public id: string;
    public containervents = new EventEmitter();
    public alarm: boolean = false;
    private _temperatureThresholds: Map<ThresholdEnum, Threshold> = new Map();
    private _alerts: Map<ThresholdEnum, AlertInterface> = new Map();
    private _temperatureSensor: SensorInterface;
    
    constructor(minTemperature: number, maxTemperature: number, id?: string){
        this.id = id;
        if (this.id == undefined){
            this.id = uuid();
        }
        this._temperatureSensor = new Sensor(this.id);
        this._addThreshold(ThresholdEnum.min, minTemperature);
        this._addThreshold(ThresholdEnum.max, maxTemperature);
    }
    
    get temperature(){
        return this._temperatureSensor.value;
    }

    get temperatureSensor(){
        return this._temperatureSensor;
    }

    get alerts(){
        return this._alerts;
    }

    get minTemperature(){
        return this._temperatureThresholds.get(ThresholdEnum.min).value;
    }

    get maxTemperature(){
        return this._temperatureThresholds.get(ThresholdEnum.max).value;
    }

    private _addThreshold(threshold: ThresholdEnum, value: number){
    
        this._temperatureThresholds.set(threshold, new Threshold(value, threshold));

        const alert = new Alert(this._temperatureThresholds.get(threshold), this.id);
        this._alerts.set(threshold, alert); 
        this._temperatureSensor.sensorEvents.on('change', this._alerts.get(threshold).verify);
        alert.alertEvents.on(Alert.ALERT_ON, this._setAlarm);
        alert.alertEvents.on(Alert.ALERT_OFF, this._setAlarm);                
        
    }

    private _setAlarm = (_id: string, state: boolean) => {
        this.alarm = state;
    }

}