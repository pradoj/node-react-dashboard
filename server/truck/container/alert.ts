import {EventEmitter} from 'events';
import { Threshold } from './threshold';
import { AlertInterface } from './alertInterface';

export class Alert implements AlertInterface{   
    static readonly ALERT_ON = 'alertOn';
    static readonly ALERT_OFF = 'alertOff';

    private _alertEvents = new EventEmitter();
    private _state = false;

    constructor(private threshold: Threshold, private elementId: string) {       
    }

    public verify = (value: number) => {     
        if (!this.threshold.validadeValue(value)){
            this._state = true;
            this._alertEvents.emit(Alert.ALERT_ON, this.elementId, this._state, new Date(), this.threshold, value);
        }else if (this._state){
            this._state = false;
            this._alertEvents.emit(Alert.ALERT_OFF, this.elementId, this._state, new Date(), this.threshold);
        }
    }

    get alertEvents(){
        return this._alertEvents
    }
  
}