import { ThresholdEnum } from './thresholdEnum';

export class Threshold{

    constructor(public value: number, public type: ThresholdEnum) {
       
    }
   
    public validadeValue(value: number):boolean{
        if ((this.type == ThresholdEnum.max && value > this.value) ||
            (this.type == ThresholdEnum.min && value < this.value)){
            return false;
        }
        return true;
    }

}