import { Container } from "../container";

export class ContainerDTO {
    public id: string;
    public minTemperature: number;
    public maxTemperature: number;
    public currentTemperature: number;
    public alarm: boolean;
    constructor(container: Container){
        this.id = container.id;        
        this.minTemperature = container.minTemperature;
        this.maxTemperature = container.maxTemperature;
        this.currentTemperature = container.temperature;
        this.alarm = container.alarm;
    }
}