import { EventEmitter } from "events";

export interface AlertInterface{
    readonly alertEvents: EventEmitter;

    verify(value:number);

}