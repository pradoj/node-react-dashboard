import {EventEmitter} from 'events';
import { SensorInterface } from './sensorInterface';

export class Sensor implements SensorInterface{   
    static readonly CHANGE = 'change';
    private _sensorEvents:EventEmitter = new EventEmitter();
    private _currentValue:number

    constructor(private id: string) {
       
    }

    set value(newValue: number){
        if (this._currentValue != newValue){
            this._currentValue = newValue;
            this._sensorEvents.emit(Sensor.CHANGE, this._currentValue, this.id);     
        }
    }

    get value(): number{
        return this._currentValue;
    }

    get sensorEvents(){
        return this._sensorEvents;
    }
}