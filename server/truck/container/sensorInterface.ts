import { EventEmitter } from "events";

export interface SensorInterface{
    readonly sensorEvents: EventEmitter;
    value: number;
    

}