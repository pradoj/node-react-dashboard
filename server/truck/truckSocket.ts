import { TruckServer } from "../truckServer";
import { Truck } from "./truck";
import { ContainerDTO } from "./container/dto/containerDTO";

export class TruckSocket{
    public static getInstance(io){
        if (!TruckSocket.instance) {         
            TruckSocket.instance = new TruckSocket(io);
        }
        return TruckSocket.instance;
    }
    private static instance: TruckSocket;
    
    private constructor(private io){
       
        Truck.getInstance().truckEvents.on(Truck.EVENT_ALERT, this.sockerALert)
    
        Truck.getInstance().truckEvents.on(Truck.EVENT_CHANGE, this.sockerChange)
    }



    emitAllContainers(){
        this.io.emit(TruckServer.EVENT_ALL_CONTAINERS, Array.from(Truck.getInstance().containers.values()).map((container) => new ContainerDTO(container)));
    };

    sockerChange = (attribute: string) => {
        if (attribute == Truck.EVENT_CHANGE_ATT_TEMPERATURE) {
            this.io.emit(TruckServer.EVENT_ALL_CONTAINERS, Array.from(Truck.getInstance().containers.values()).map((container) => new ContainerDTO(container)));
        }
    }

    sockerALert = (id: string, active: boolean, value: number, _date: Date) => {
        if (active) {
            this.io.emit('alert', { message: `container ${id} has temperature as ${value} which is not on current range.` });
        }
    }

    
}