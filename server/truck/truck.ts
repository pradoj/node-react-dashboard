import { Container } from './container/container';
import { Alert } from './container/alert';
import { EventEmitter } from 'events';
import { Threshold } from './container/threshold';
import { Sensor } from './container/sensor';

export class Truck{
    public static readonly EVENT_ALERT = 'alert';
    public static readonly EVENT_CHANGE = 'change'; 
    public static readonly EVENT_CHANGE_ATT_TEMPERATURE = 'temperature'; 
    truckEvents: EventEmitter = new EventEmitter();
    _containers: Map<string, Container> = new Map();

    private static instance: Truck;
    private constructor() {
        // do something construct...
    }

    static getInstance(): Truck {
        if (!Truck.instance) {         
            Truck.instance = new Truck();
        }
        return Truck.instance;
    }

    get containers(): Map<string, Container> {
        return  this._containers;
    }

    getContainer(id: string): Container {
        return this._containers.get(id);
    }
    
    addContainer(container: Container){
        this._containers.set(container.id, container);
        container.temperatureSensor.sensorEvents.on(Sensor.CHANGE, this.onChange);
        container.alerts.forEach(
            (alert) => {
                alert.alertEvents.on(Alert.ALERT_ON, this.alert);
                         
        });
        
    }

    alert = (id: string, _status: boolean, date: Date, threshold: Threshold, value: number) =>{        
        this.truckEvents.emit(Truck.EVENT_ALERT, id, true, value, date, threshold);
    }
    
    onChange = (value: number, id: String) => {
        this.truckEvents.emit(Truck.EVENT_CHANGE, Truck.EVENT_CHANGE_ATT_TEMPERATURE , id, value);
    }
    
}