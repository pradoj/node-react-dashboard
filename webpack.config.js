const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: "./app/index.tsx",
    output: {
        filename: "bundle.js",
        path: __dirname + "/public/js"
    },
    plugins: [
        new HtmlWebpackPlugin({template: 'index.html', filename: "../index.html"})
      ],
    devtool: "source-map",

    resolve: {
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        rules: [
            
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            { test: /\.tsx?$/, loader: "awesome-typescript-loader"
            }
        ]
    }
};
