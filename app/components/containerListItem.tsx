import * as React from 'react'
import * as classNames from 'classnames'

export interface Container {
    id: string
    temperature: number
    status: boolean
    minTemperature: number
    maxTemperature: number
}

export class ContainerListItem extends React.Component<Container, {}> {
    constructor(){
        super(null);
       
    }

    hasAlerts() {
        return this.props.status
    }

    render() {
        const panelClass = this.hasAlerts() ? 'success' : 'default'
        const classes = classNames('panel', `panel-${panelClass}`)
    
        return (
            <div className="col-sm-3">
                <div className={ classes }>
                    <div className="panel-heading">{ this.props.id }</div>
                    <div className="panel-body">
                        temperature: {this.props.temperature} <br/>
                        Range: {this.props.minTemperature} - {this.props.maxTemperature}                        
                    </div>
                    
                </div>
            </div>
        )
    }
}