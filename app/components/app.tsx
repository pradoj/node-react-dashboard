import * as React from 'react'
import { Container } from './containerListItem'
import { ContainerList } from './containerList'
import * as _ from 'lodash'
import * as io from 'socket.io-client'

let socket:SocketIOClient.Socket = io.connect()

class AppState {
    containers?: Container[]
    alerts?: Container[]
}

export class AppComponent extends React.Component<{}, AppState> {

    constructor() {
        super({});
        
        this.state = {
            containers: []
        }
        socket.on('containers.all', (containers: any) => {   
            this.setState({
                containers: containers.map(this.mapContainer)
            })
        })
        socket.on('alert', (args: any) => {
            alert(args.message)
        })
        socket.emit('containers.all');

    }
    
    mapContainer(container:any): Container {
        return {
            id: container.id,
            status: !container.alarm,
            temperature: container.currentTemperature,
            minTemperature: container.minTemperature,
            maxTemperature: container.maxTemperature
        }
    }

    render() {
        return (
            <div className="container">
                <h1 className="page-header">PragmaBrewery Dashboard</h1>
                <ContainerList containers={this.state.containers} />
            </div>
        )
    }
}