import * as React from 'react'
import { Container, ContainerListItem } from './containerListItem'

export class ContainerListProps {
    containers: Container[]
   
}

export class ContainerList extends React.Component<ContainerListProps, any> {
    render() {
        return (
            <div>                
                <p>{ this.props.containers.length == 0 ? "No containers to show" : "" }</p>
                <div className="row">
                    { this.props.containers.map(c => <ContainerListItem  key={c.id} {...c} />)  }
                </div>
            </div>
        )
    }
}