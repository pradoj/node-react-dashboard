# Brewery Containers Dashboard

This is the code repository for the create a beer containers dashboard with Node, Typescript, React and Express and Socket.io.

## Prerequisites

```
npm install
npm link typescript
```

## Code
web code on ./app
server code on ./server

## Assumptions
As advised, the ui is just to display the behaviour of the server so it is simple.
I used Typescript on the top of JS on node and react.

## Improviments
The solution could have better events between server and clients. For exemplo refresh specific container or even properties when they change instead of refreshing all containers.
It would required the UI components to share the same socketio conection in order to not have unnecessary openned connections and the server to be more complex.

## Observation
Would be good to receive an boilerplate with the baseline project. 

## Build
```
npm run build
```

## Running
```
npm start
```

## Updating temperature:
PUT request to http://localhost:3000/api/v1/truck/containers/{containerID}/temperatureSensor

Sample: 
http://localhost:3000/api/v1/truck/containers/0324aad0-336c-11e9-b877-1b9ad917432a/temperatureSensor
Body content type:
json
body content
{"value":3}

## Test/Coverage
```
npm test
```
